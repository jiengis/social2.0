package com.social2.Factory.dao;

import com.social2.dao.Int.DaoCommentoInt;
import com.social2.dao.Int.DaoPaginaInt;
import com.social2.dao.Int.DaoUtenteInt;
import com.social2.dao.impl.DaoCommentoImpl;
import com.social2.dao.impl.DaoPaginaImpl;
import com.social2.dao.impl.DaoUtenteImpl;
import com.social2.reports.Report;
import com.social2.reports.Int.ReportInt;

public class FactoryDao {

	private volatile static DaoCommentoInt daoCommentoInt;
	private volatile static DaoPaginaInt daoPaginaInt;
	private volatile static DaoUtenteInt daoUtenteInt;
	private volatile static ReportInt reportInt;

	public static ReportInt getReportInt() {
		
		if (reportInt == null) {
			synchronized (FactoryDao.class) {
				if (reportInt == null) {
					reportInt = new Report();
				}
			}
		}
		return reportInt;
	}

	public static DaoCommentoInt getDaoCommentoInt() {
		
		if (daoCommentoInt == null) {
			synchronized (FactoryDao.class) {
				if (daoCommentoInt == null) {
					daoCommentoInt = new DaoCommentoImpl();
				}
			}
		}
		return daoCommentoInt;
	}

	public static DaoPaginaInt getDaoPaginaInt() {
		
		if (daoPaginaInt == null) {
			synchronized (FactoryDao.class) {
				if (daoPaginaInt == null) {
					daoPaginaInt = new DaoPaginaImpl();
				}
			}
		}
		return daoPaginaInt;
	}

	public static DaoUtenteInt getDaoUtenteInt() {
		
		if (daoUtenteInt == null) {
			synchronized (FactoryDao.class) {
				if (daoUtenteInt == null) {
					daoUtenteInt = new DaoUtenteImpl();
				}
			}
		}
		return daoUtenteInt;
	}

}