package com.social2.Factory.service;

import com.social2.reports.service.Int.ServiceReportsInt;
import com.social2.reports.service.impl.ServiceReportsImpl;
import com.social2.service.Int.ServiceCommentoInt;
import com.social2.service.Int.ServiceLoginInt;
import com.social2.service.Int.ServicePaginaInt;
import com.social2.service.Int.ServiceUtenteInt;
import com.social2.service.impl.ServiceCommentoImpl;
import com.social2.service.impl.ServiceLoginImpl;
import com.social2.service.impl.ServicePaginaImpl;
import com.social2.service.impl.ServiceUtenteImpl;

public class FactoryService {

	private volatile static ServiceLoginInt loginServiceInt;
	private volatile static ServicePaginaInt servicePaginaInt;
	private volatile static ServiceCommentoInt serviceCommentoInt;
	private volatile static ServiceUtenteInt serviceUtenteInt;
	private volatile static ServiceReportsInt serviceReportsInt;
	
	
	public static ServiceReportsInt getServiceReportsInt() {
		
		if(serviceReportsInt == null) {
			synchronized (FactoryService.class) {
				if (serviceReportsInt == null) {
					serviceReportsInt = new ServiceReportsImpl();
				}
			}
		}
		return serviceReportsInt;
	}
	
	public static ServiceLoginInt getLoginServiceInt() {
		
		if(loginServiceInt == null) {
			synchronized (FactoryService.class) {
				if (loginServiceInt == null) {
					loginServiceInt = new ServiceLoginImpl();
				}
			}
		}
		return loginServiceInt;
	}
	
	public static ServicePaginaInt getServicePaginaInt() {
		
		if(servicePaginaInt == null) {
			synchronized (FactoryService.class) {
				if (servicePaginaInt == null) {
					servicePaginaInt = new ServicePaginaImpl();
				}
			}
		}
		return servicePaginaInt;
	}
	
	public static ServiceCommentoInt getServiceCommentoInt() {
		
		if(serviceCommentoInt == null) {
			synchronized (FactoryService.class) {
				if (serviceCommentoInt == null) {
					serviceCommentoInt = new ServiceCommentoImpl();
				}
			}
		}
		return serviceCommentoInt;
	}
	
	public static ServiceUtenteInt getServiceUtenteInt() {
		
		if(serviceUtenteInt == null) {
			synchronized (FactoryService.class) {
				if (serviceUtenteInt == null) {
					serviceUtenteInt = new ServiceUtenteImpl();
				}
			}
		}
		return serviceUtenteInt;
	}
	
	
}
