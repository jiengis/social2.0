package com.social2.model;

import java.sql.Date;

public class Pagina {

	private int idpagina;
	private String titolo;
	private String testo;
	private int idutente;
	private Date dataIns;
	
	
	public Pagina() {
		
	}

	public int getIdpagina() {
		return idpagina;
	}

	public void setIdpagina(int idpagina) {
		this.idpagina = idpagina;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getTesto() {
		return testo;
	}

	public void setTesto(String testo) {
		this.testo = testo;
	}

	public Date getDataIns() {
		return dataIns;
	}

	public void setDataIns(Date dataIns) {
		this.dataIns = dataIns;
	}
	
	public int getIdutente() {
		return idutente;
	}

	public void setIdutente(int idutente) {
		this.idutente = idutente;
	}
}
