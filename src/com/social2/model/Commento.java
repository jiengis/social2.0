package com.social2.model;

public class Commento {

	private int idcommento;
	private String testo;
	private int idpagina;
	private int idutente;
	
	public Commento() {
		
	}

	public String getTesto() {
		return testo;
	}

	public void setTesto(String testo) {
		this.testo = testo;
	}

	public int getIdpagina() {
		return idpagina;
	}

	public void setIdpagina(int idpagina) {
		this.idpagina = idpagina;
	}

	public int getIdutente() {
		return idutente;
	}

	public void setIdutente(int idutente) {
		this.idutente = idutente;
	}

	public int getIdcommento() {
		return idcommento;
	}

	public void setIdcommento(int idcommento) {
		this.idcommento = idcommento;
	}
	
	
}
