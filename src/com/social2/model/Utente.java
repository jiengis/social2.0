package com.social2.model;

public class Utente {

	private int idutente;
	private String nome;
	private String cognome;
	private String immagine;
	
	public Utente() {
		
	}

	public int getIdutente() {
		return idutente;
	}

	public void setIdutente(int idutente) {
		this.idutente = idutente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}

	@Override
	public String toString() {
		return "Utente [idutente=" + idutente + ", nome=" + nome + ", cognome=" + cognome + ", immagine=" + immagine
				+ "]";
	}
	
	
}
