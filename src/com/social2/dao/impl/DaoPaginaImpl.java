package com.social2.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.social2.dao.DaoGeneric;
import com.social2.dao.Int.DaoPaginaInt;
import com.social2.model.Pagina;

public class DaoPaginaImpl implements DaoPaginaInt {

	@Override
	public ArrayList<Pagina> getPagineUtente(int idutente) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rst = null;
		ArrayList<Pagina> pagine = new ArrayList<Pagina>();
		String sql = "SELECT * FROM pagine where idutente=? order by dataInserimento";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, idutente);
			rst = pst.executeQuery();
			while(rst.next()) {
				Pagina pagina = new Pagina();
				pagina.setIdpagina(rst.getInt("idpagina"));
				pagina.setTitolo(rst.getString("titoloPagina"));
				pagina.setTesto(rst.getString("testoPagina"));
				pagina.setIdutente(rst.getInt("idutente"));
				pagina.setDataIns(rst.getDate("dataInserimento"));
				pagine.add(pagina);
			}
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (rst != null) {
				try {
					rst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}
		return pagine;
	}

	@Override
	public ArrayList<Pagina> getAllPagesByDate() {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rst = null;
		ArrayList<Pagina> pagine = new ArrayList<Pagina>();
		String sql = "SELECT * FROM pagine order by dataInserimento";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			rst = pst.executeQuery();
			while(rst.next()) {
				Pagina pagina = new Pagina();
				pagina.setIdpagina(rst.getInt("idpagina"));
				pagina.setIdutente(rst.getInt("idutente"));
				pagina.setTitolo(rst.getString("titoloPagina"));
				pagina.setTesto(rst.getString("testoPagina"));
				pagina.setDataIns(rst.getDate("dataInserimento"));
				pagine.add(pagina);
			}
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (rst != null) {
				try {
					rst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}
		return pagine;
	}

	@Override
	public void createPagina(String titolo, String testo, int idutente, Date dataIns) {
		Connection conn = null;
		PreparedStatement pst = null;
		String sql = "insert into pagine (titoloPagina,testoPagina,idutente, dataInserimento) values (?,?,?,?)";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, titolo);
			pst.setString(2, testo);
			pst.setInt(3, idutente);
			pst.setDate(4, dataIns);
			pst.executeUpdate();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}	
		
	}

	@Override
	public void delPagina(int idpagina) {
		Connection conn = null;
		PreparedStatement pst = null;
		String sql = "delete from pagine where idpagina=?";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, idpagina);
			pst.executeUpdate();
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}
	}

	@Override
	public Pagina getPagina(int idpagina) {
		Pagina pagina = new Pagina();
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rst = null;
		String sql = "select * from pagine where idpagina=?";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, idpagina);
			rst = pst.executeQuery();
			rst.first();
			pagina.setIdpagina(rst.getInt("idpagina"));
			pagina.setTitolo(rst.getString("titoloPagina"));
			pagina.setTesto(rst.getString("testoPagina"));
			pagina.setIdutente(rst.getInt("idutente"));
			pagina.setDataIns(rst.getDate("dataInserimento"));
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}
		return pagina;
	}

}
