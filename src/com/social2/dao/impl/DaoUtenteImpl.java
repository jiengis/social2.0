package com.social2.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.social2.dao.DaoGeneric;
import com.social2.dao.Int.DaoUtenteInt;
import com.social2.model.Utente;

public class DaoUtenteImpl implements DaoUtenteInt {

	@Override
	public Utente getUtente(String nome, String cognome) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rst = null;
		Utente utente = new Utente();
		String sql = "SELECT * FROM utenti where nome=? and cognome=?";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, nome);
			pst.setString(2, cognome);
			rst = pst.executeQuery();
			rst.first();
			if(rst != null) {
				utente.setIdutente(rst.getInt("idutente"));
				utente.setNome(rst.getString("nome"));
				utente.setCognome(rst.getString("cognome"));
				utente.setImmagine(rst.getString("immagine"));
			}
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (rst != null) {
				try {
					rst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}
		return utente;
	}
	
	@Override
	public ArrayList<Utente> getUtenti() {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rst = null;
		ArrayList<Utente> utenti = new ArrayList<Utente>();
		String sql = "select * from utenti order by nome";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			rst = pst.executeQuery();
			while(rst.next()) {
				Utente utente = new Utente();
				utente.setIdutente(rst.getInt("idutente"));
				utente.setNome(rst.getString("nome"));
				utente.setCognome(rst.getString("cognome"));
				utente.setImmagine(rst.getString("immagine"));
				utenti.add(utente);
			}
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (rst != null) {
				try {
					rst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}
		return utenti;
	}

	@Override
	public Utente getUtente(int id) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rst = null;
		Utente utente = new Utente();
		String sql = "SELECT * FROM utenti where idutente=?";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			rst = pst.executeQuery();
			rst.first();
			if(rst != null) {
				utente.setIdutente(rst.getInt("idutente"));
				utente.setNome(rst.getString("nome"));
				utente.setCognome(rst.getString("cognome"));
				utente.setImmagine(rst.getString("immagine"));
			}
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (rst != null) {
				try {
					rst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}
		return utente;
	}

}
