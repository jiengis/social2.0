package com.social2.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.social2.dao.DaoGeneric;
import com.social2.dao.Int.DaoCommentoInt;
import com.social2.model.Commento;

public class DaoCommentoImpl implements DaoCommentoInt {

	@Override
	public ArrayList<Commento> getCommentiPagina(int idpagina) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rst = null;
		ArrayList<Commento> commenti = new ArrayList<Commento>();
		String sql = "SELECT * from commenti where idpagina=?";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, idpagina);
			rst = pst.executeQuery();
			while(rst.next()) {
				Commento commento = new Commento();
				commento.setIdcommento(rst.getInt("idcommento"));
				commento.setIdpagina(rst.getInt("idpagina"));
				commento.setIdutente(rst.getInt("idutente"));
				commento.setTesto(rst.getString("testo"));
				commenti.add(commento);
			}
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (rst != null) {
				try {
					rst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}
		return commenti;
	}

	@Override
	public void createCommento(String testo, int idutente, int idpagina) {
		Connection conn = null;
		PreparedStatement pst = null;
		String sql = "insert into commenti (testo,idutente,idpagina) values(?,?,?)";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, testo);
			pst.setInt(2, idutente);
			pst.setInt(3, idpagina);
			pst.executeUpdate();
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}	
	}

	@Override
	public ArrayList<Commento> getCommentiUtente(int idutente) {
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rst = null;
		ArrayList<Commento> commenti = new ArrayList<Commento>();
		String sql = "SELECT * from commenti where idutente=?";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, idutente);
			rst = pst.executeQuery();
			while(rst.next()) {
				Commento commento = new Commento();
				commento.setIdcommento(rst.getInt("idcommento"));
				commento.setIdutente(rst.getInt("idutente"));
				commento.setIdpagina(rst.getInt("idpagina"));
				commento.setTesto(rst.getString("testo"));
				commenti.add(commento);
			}
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (rst != null) {
				try {
					rst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}
		return commenti;
	}

	@Override
	public ArrayList<Commento> getAllCommenti() {
			Connection conn = null;
			PreparedStatement pst = null;
			ResultSet rst = null;
			ArrayList<Commento> commenti = new ArrayList<Commento>();
			String sql = "SELECT * from commenti";
			try {
				conn = DaoGeneric.getConnection();
				pst = conn.prepareStatement(sql);
				rst = pst.executeQuery();
				while(rst.next()) {
					Commento commento = new Commento();
					commento.setIdcommento(rst.getInt("idcommento"));
					commento.setIdpagina(rst.getInt("idpagina"));
					commento.setIdutente(rst.getInt("idutente"));
					commento.setTesto(rst.getString("testo"));
					commenti.add(commento);
				}
			}
			catch(Exception e) {
				throw new RuntimeException(e);
			}
			finally {
				if (rst != null) {
					try {
						rst.close();
					} catch (SQLException sqle) {
						sqle.printStackTrace();
					}
				}
				if (pst != null) {
					try {
						pst.close();
					} catch (SQLException sqle) {
						sqle.printStackTrace();
					}
				}
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException sqle) {
						sqle.printStackTrace();
					}
				}
			}
			return commenti;
	}

	@Override
	public void delCommento(int idcommento) {
		Connection conn = null;
		PreparedStatement pst = null;
		String sql = "delete from commenti where idcommento=?";
		try {
			conn = DaoGeneric.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, idcommento);
			pst.executeUpdate();
		}
		catch(Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
				}
			}
		}
	}
	
}
