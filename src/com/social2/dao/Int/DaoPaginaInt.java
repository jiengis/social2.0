package com.social2.dao.Int;

import java.sql.Date;
import java.util.ArrayList;

import com.social2.model.Pagina;

public interface DaoPaginaInt {

	//4 - elencare le pagine create da un utente (paginazione + ordinamento per data + per titolo + numero commenti)
	public ArrayList<Pagina> getPagineUtente(int idutente);
	
	//3 - elencare le pagine create da tutti gli utenti (paginazione + ordinamento per data + numero commenti)
	public ArrayList<Pagina> getAllPagesByDate();
	
	public void createPagina(String titolo, String testo, int idutente, Date dataIns);
	public void delPagina(int idpagina);
	public Pagina getPagina(int idpagina);
}
