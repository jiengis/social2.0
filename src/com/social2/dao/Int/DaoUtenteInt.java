package com.social2.dao.Int;

import java.util.ArrayList;

import com.social2.model.Utente;

public interface DaoUtenteInt {

	public Utente getUtente(String nome, String cognome);
	
	public Utente getUtente(int id);
	
	//1 - elencare tutti gli utenti (paginazione + ordinamento per nome)
	public ArrayList<Utente> getUtenti();
}
