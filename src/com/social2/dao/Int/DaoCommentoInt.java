package com.social2.dao.Int;

import java.util.ArrayList;

import com.social2.model.Commento;

public interface DaoCommentoInt {
	
	//2 - visualizzare una pagina con i relativi commenti
	public ArrayList<Commento> getCommentiPagina(int idpagina);
	public ArrayList<Commento> getCommentiUtente(int idutente);
	public void createCommento(String testo, int idutente, int idpagina);
	public ArrayList<Commento> getAllCommenti();
	public void delCommento(int idcommento);
	
}
