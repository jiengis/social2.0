package com.social2.dao;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DaoGeneric {

	private static DataSource ds = null;

	private static DataSource getDataSource() throws Exception {

		if (ds == null) {
			synchronized (DaoGeneric.class) {
				if (ds == null) {
					Context initCtx;
					Context envCtx = null;
					initCtx = new InitialContext();
					envCtx = (Context) initCtx.lookup("java:comp/env");
					ds = (DataSource) envCtx.lookup("jdbc/keygram2");
				}
			}
		}
		return ds;
	}

	public static Connection getConnection() throws Exception {
		return getDataSource().getConnection();
	}

}