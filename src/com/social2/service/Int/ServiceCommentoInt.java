package com.social2.service.Int;

import java.util.ArrayList;

import com.social2.model.Commento;

public interface ServiceCommentoInt {

	public void insertCommento(String testo,int idutente, int idpagina);
	public void deleteCommento(int idcommento);
	public ArrayList<Commento> getCommentiXPagina(int idpagina);
	public ArrayList<Commento> getCommentiXUtente(int idutente);
	public ArrayList<Commento> getAllComments();
	
}
