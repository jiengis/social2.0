package com.social2.service.Int;

import java.sql.Date;
import java.util.ArrayList;

import com.social2.model.Pagina;

public interface ServicePaginaInt {

	public ArrayList<Pagina> pagineXutente(int idutente);
	
	public void addPagina(String titolo, String testo, int idutente, Date dataIns);
	public ArrayList<Pagina> getPagine();
	public void deletePagina(int idpagina);
	public Pagina getPage(int idpagina);
}
