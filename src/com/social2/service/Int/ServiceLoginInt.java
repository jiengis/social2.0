package com.social2.service.Int;

import com.social2.model.Utente;

public interface ServiceLoginInt {

	public Utente isAuthenticated(String nome,String cognome);
}