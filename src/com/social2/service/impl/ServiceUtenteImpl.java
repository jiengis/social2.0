package com.social2.service.impl;

import java.util.ArrayList;

import com.social2.Factory.dao.FactoryDao;
import com.social2.model.Utente;
import com.social2.service.Int.ServiceUtenteInt;

public class ServiceUtenteImpl implements ServiceUtenteInt {

	public ArrayList<Utente> getUsers(){
		return FactoryDao.getDaoUtenteInt().getUtenti();
		
	}
}
