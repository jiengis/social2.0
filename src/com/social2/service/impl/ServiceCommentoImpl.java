package com.social2.service.impl;

import java.util.ArrayList;

import com.social2.Factory.dao.FactoryDao;
import com.social2.model.Commento;
import com.social2.service.Int.ServiceCommentoInt;

public class ServiceCommentoImpl implements ServiceCommentoInt {

	@Override
	public void insertCommento(String testo, int idutente, int idpagina) {
		FactoryDao.getDaoCommentoInt().createCommento(testo, idutente, idpagina);
	}
	
	@Override
	public ArrayList<Commento> getCommentiXPagina(int idpagina){
		return FactoryDao.getDaoCommentoInt().getCommentiPagina(idpagina);
	}

	@Override
	public ArrayList<Commento> getCommentiXUtente(int idutente) {
		return FactoryDao.getDaoCommentoInt().getCommentiUtente(idutente);
	}

	@Override
	public ArrayList<Commento> getAllComments() {
		return FactoryDao.getDaoCommentoInt().getAllCommenti();
	}

	@Override
	public void deleteCommento(int idcommento) {
		FactoryDao.getDaoCommentoInt().delCommento(idcommento);
	}

}
