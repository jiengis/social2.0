package com.social2.service.impl;

import com.social2.Factory.dao.FactoryDao;
import com.social2.model.Utente;
import com.social2.service.Int.ServiceLoginInt;

public class ServiceLoginImpl implements ServiceLoginInt {

	@Override
	public Utente isAuthenticated(String nome, String cognome) {
		return FactoryDao.getDaoUtenteInt().getUtente(nome, cognome);
	}

}
