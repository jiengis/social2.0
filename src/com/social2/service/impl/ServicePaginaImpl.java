package com.social2.service.impl;

import java.sql.Date;
import java.util.ArrayList;

import com.social2.Factory.dao.FactoryDao;
import com.social2.model.Pagina;
import com.social2.service.Int.ServicePaginaInt;

public class ServicePaginaImpl implements ServicePaginaInt {

	@Override
	public ArrayList<Pagina> pagineXutente(int idutente) {
		return FactoryDao.getDaoPaginaInt().getPagineUtente(idutente);
	}

	@Override
	public void addPagina(String titolo, String testo, int idutente, Date dataIns) {
		FactoryDao.getDaoPaginaInt().createPagina(titolo, testo, idutente, dataIns);
	}

	@Override
	public ArrayList<Pagina> getPagine() {
		return FactoryDao.getDaoPaginaInt().getAllPagesByDate();
	}

	@Override
	public void deletePagina(int idpagina) {
		FactoryDao.getDaoCommentoInt().delCommento(idpagina);
	}

	@Override
	public Pagina getPage(int idpagina) {
		return FactoryDao.getDaoPaginaInt().getPagina(idpagina);
	}

}
