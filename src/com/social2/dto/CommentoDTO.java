package com.social2.dto;


import com.social2.Factory.dao.FactoryDao;
import com.social2.model.Commento;
import com.social2.model.Utente;

public class CommentoDTO {
	
	public CommentoDTO(Commento commento) {
		
		this.testo = commento.getTesto();
		this.idpagina = commento.getIdpagina();

		Utente scrittore = FactoryDao.getDaoUtenteInt().getUtente(commento.getIdutente());		
		
		if( scrittore != null ) 
			this.scrittore = scrittore.getCognome()+" "+scrittore.getNome();
		else
			this.scrittore = "";
		
	}
	
	private final String testo;
	private final Integer idpagina;
	private final String scrittore;
	
	public String getTesto() {
		return testo;
	}
	public int getIdpagina() {
		return idpagina;
	}
	public String getScrittore() {
		return scrittore;
	}
}
