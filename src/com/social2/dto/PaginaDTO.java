package com.social2.dto;

import java.text.SimpleDateFormat;

import com.social2.Factory.dao.FactoryDao;
import com.social2.model.Pagina;
import com.social2.model.Utente;

public class PaginaDTO {

	public PaginaDTO(Pagina pagina) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		this.idpagina = pagina.getIdpagina();
		this.testo = pagina.getTesto();
		this.titolo = pagina.getTitolo();
		this.dataIns = sdf.format(pagina.getDataIns());
		
		Utente autore = FactoryDao.getDaoUtenteInt().getUtente(pagina.getIdutente());
		
		if (autore != null)
			this.autore = autore.getCognome() + " " + autore.getNome();
		else
			this.autore = "";
	}

	private final Integer idpagina;
	private final String titolo;
	private final String testo;
	private final String autore;
	private final String dataIns;

	public String getDataIns() {
		return dataIns;
	}

	public Integer getIdpagina() {
		return idpagina;
	}

	public String getTitolo() {
		return titolo;
	}

	public String getTesto() {
		return testo;
	}

	public String getAutore() {
		return autore;
	}

}
