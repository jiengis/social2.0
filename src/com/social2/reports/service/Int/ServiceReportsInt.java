package com.social2.reports.service.Int;


public interface ServiceReportsInt {

	public byte[] printCSVutenti();
	public byte[] printCSVpagine();
	public byte[] printCSVcommenti();
}
