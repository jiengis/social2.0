package com.social2.reports.service.impl;


import com.social2.Factory.dao.FactoryDao;
import com.social2.reports.service.Int.ServiceReportsInt;

public class ServiceReportsImpl implements ServiceReportsInt {

	@Override
	public byte[] printCSVutenti() {
		return FactoryDao.getReportInt().getUtentiCSV();
		
	}

	@Override
	public byte[] printCSVpagine() {
		return FactoryDao.getReportInt().getPagineCSV();
		
	}

	@Override
	public byte[] printCSVcommenti() {
		return FactoryDao.getReportInt().getCommentiCSV();
		
	}

}
