package com.social2.reports.Int;


public interface ReportInt {

	public byte[] getUtentiCSV();
	public byte[] getPagineCSV();
	public byte[] getCommentiCSV();
}
