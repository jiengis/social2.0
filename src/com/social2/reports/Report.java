package com.social2.reports;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.social2.dao.DaoGeneric;
import com.social2.reports.Int.ReportInt;

public class Report implements ReportInt {

	
	public byte[] getUtentiCSV() {
		String sql = "select * from utenti";
		StringBuilder sb = new StringBuilder();
		sb.append("IDUTENTE,NOME,COGNOME,IMMAGINE\n");
		try ( //
				Connection conn = DaoGeneric.getConnection(); //
				PreparedStatement pst = conn.prepareStatement(sql); //
				ResultSet rst = pst.executeQuery(); //
		) {
			while (rst.next()) {
				
				sb.append(rst.getInt("idutente"));
				sb.append(",");
				
				sb.append(rst.getString("nome"));
				sb.append(",");
				
				sb.append(rst.getString("cognome"));
				sb.append(",");
				
				sb.append(rst.getString("immagine"));
				sb.append("\n");
			}
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return sb.toString().getBytes();
	}

	
	
	public byte[] getPagineCSV() {
		String sql = "select * from pagine";
		StringBuilder sb = new StringBuilder();
		sb.append("IDPAGINA,TITOLO,TESTO,IDUTENTE,DATAINSERIMENTO\n");
		try ( //
				Connection conn = DaoGeneric.getConnection(); //
				PreparedStatement pst = conn.prepareStatement(sql); //
				ResultSet rst = pst.executeQuery(); //
		) {
			while (rst.next()) {
				
				sb.append(rst.getInt("idpagina"));
				sb.append(",");
				
				sb.append(rst.getString("titoloPagina"));
				sb.append(",");
				
				sb.append(rst.getString("testoPagina"));
				sb.append(",");
				
				sb.append(rst.getString("idutente"));
				sb.append(",");
				
				sb.append(rst.getString("dataInserimento"));
				sb.append("\n");
			}
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return sb.toString().getBytes();
	}

	
	
	public byte[] getCommentiCSV() {
		String sql = "select * from commenti";
		StringBuilder sb = new StringBuilder();
		sb.append("IDCOMMENTO,TESTO,IDPAGINA,IDUTENTE\n");
		try ( //
				Connection conn = DaoGeneric.getConnection(); //
				PreparedStatement pst = conn.prepareStatement(sql); //
				ResultSet rst = pst.executeQuery(); //
		) {
			while (rst.next()) {
				
				sb.append(rst.getInt("idcommento"));
				sb.append(",");
				
				sb.append(rst.getString("testo"));
				sb.append(",");
				
				sb.append(rst.getInt("idpagina"));
				sb.append(",");
				
				sb.append(rst.getInt("idutente"));
				sb.append("\n");
			}
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return sb.toString().getBytes();
	}
}
