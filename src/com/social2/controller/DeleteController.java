package com.social2.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.social2.Factory.service.FactoryService;


@WebServlet("/DeleteController")
public class DeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public DeleteController() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		if("cancellaCommento".equals(request.getParameter("cancellaCommento"))) {
			FactoryService.getServiceCommentoInt().deleteCommento(Integer.parseInt(request.getParameter("idCommento")));
		}
		
		if("cancellaPagina".equals(request.getParameter("cancellaPagina"))) {
			FactoryService.getServicePaginaInt().deletePagina(Integer.parseInt(request.getParameter("idPag")));
		}
	}

}
