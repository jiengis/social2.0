package com.social2.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.social2.Factory.service.FactoryService;
import com.social2.model.Utente;


@WebServlet("/UtentiController")
public class UtentiController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

    public UtentiController() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());

		if(request.getParameter("idUtente") != null) {
			int x = Integer.parseInt(request.getParameter("idUtente"));
			ArrayList<Utente> utenti = FactoryService.getServiceUtenteInt().getUsers();
				for(Utente utente : utenti) {
					if(utente.getIdutente() == x) {
						request.setAttribute("utente", utente);
						request.getRequestDispatcher("/WEB-INF/utente.jsp").forward(request, response);
						System.out.println(utente.toString());
					}
				}
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		 ArrayList<Utente> utenti = FactoryService.getServiceUtenteInt().getUsers();
		 request.setAttribute("utenti", utenti);
		 request.getRequestDispatcher("utenti.jsp").forward(request, response);
	}

}
