package com.social2.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.social2.Factory.service.FactoryService;
import com.social2.dto.CommentoDTO;
import com.social2.dto.PaginaDTO;
import com.social2.model.Commento;
import com.social2.model.Pagina;

@WebServlet("/PaginaController")
public class PaginaController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public PaginaController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int idpagina = Integer.parseInt(request.getParameter("idPagina"));
		Pagina pagina = FactoryService.getServicePaginaInt().getPage(idpagina);
		ArrayList<Commento> commenti = FactoryService.getServiceCommentoInt().getCommentiXPagina(idpagina);
		ArrayList<CommentoDTO> commentiDTO = new ArrayList<CommentoDTO>();
		for (Commento commento : commenti ) {
			CommentoDTO commentoDTO = new CommentoDTO(commento);
			commentiDTO.add(commentoDTO);
		}
		PaginaDTO paginaDTO = new PaginaDTO(pagina);
		request.setAttribute("pagina", paginaDTO);
		request.setAttribute("CommentiPagina", commentiDTO);
		
		request.getRequestDispatcher("/WEB-INF/pagina.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//doGet(request, response);

		ArrayList<Pagina> pagine = FactoryService.getServicePaginaInt().getPagine();
		request.setAttribute("allPagine", pagine);
		request.getRequestDispatcher("pagine.jsp").forward(request, response);

	}

}
