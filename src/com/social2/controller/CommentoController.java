package com.social2.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.social2.Factory.service.FactoryService;
import com.social2.model.Utente;


@WebServlet("/CommentoController")
public class CommentoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CommentoController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int x = Integer.parseInt(request.getParameter("aggiungiCommento"));
		if( x != 0) {
			Utente utente = (Utente) request.getSession().getAttribute("utenteLog");
			FactoryService.getServiceCommentoInt().insertCommento(
							request.getParameter("testoc"),
							utente.getIdutente(),
							Integer.parseInt(request.getParameter("aggiungiCommento"))
							);
			
			response.sendRedirect("PaginaController?idPagina="+x + "#formCommento");
		}
	}

}
