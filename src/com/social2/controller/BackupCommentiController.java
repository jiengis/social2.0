package com.social2.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.social2.Factory.service.FactoryService;


@WebServlet("/BackupCommenti")
public class BackupCommentiController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename=downloadcommenti.csv");
		
		ServletOutputStream outStream = response.getOutputStream();
		outStream.write(FactoryService.getServiceReportsInt().printCSVcommenti());
	}

}
