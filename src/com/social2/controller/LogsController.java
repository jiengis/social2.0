package com.social2.controller;

import java.io.IOException;
//import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.social2.Factory.service.FactoryService;
import com.social2.model.Utente;

/**
 * Servlet implementation class LogsController
 */
@WebServlet("/LogsController")
public class LogsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LogsController() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		
		if("login".equals(request.getParameter("login"))) {
			Utente utente = FactoryService.getLoginServiceInt().isAuthenticated(request.getParameter("nome"),
																			request.getParameter("cognome"));
				HttpSession session = request.getSession();
				session.setAttribute("utenteLog", utente);
				response.sendRedirect("home.jsp");
		}
		
		if("logout".equals(request.getParameter("logout"))) {
			if(request.getSession() != null) {
				request.getSession().invalidate();
				response.sendRedirect("index.jsp");
			}
		}

	}

}
