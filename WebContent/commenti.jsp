
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>

<strong>COMMENTI</strong>
<br>
<br>

<c:forEach items="${CommentiPagina}" var="commento">

	<table border=1 style="text-align: center;" width="60%">
		<tr>
			<th width="50%">Testo</th>
			<th width="50%">Scritto da</th>
		</tr>
		<tr>
			<td width="50%"><c:out value="${commento.testo}" /></td>
			<td width="50%"><c:out value="${commento.scrittore}" /></td>
		</tr>
	</table>
	<br>
	<br>
</c:forEach>

