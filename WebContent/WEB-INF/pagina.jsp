<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Pagina ${pagina.idpagina}</h1>
	<form method="post" action="LogsController">
		<input type="hidden" name="logout" value="logout">
		<input type="submit" value="Logout">
	</form>
	<!-- logout ok -->
	<br>
	<a href="home.jsp">Home</a>
	<br>
	<hr>
	<table style="text-align: center;" border=1 width="100%">
		<tr>
			<td width="30%">Titolo</td>
			<td width="30%">Autore</td>
			<td width="30%">Data</td>
		</tr>
		<tr>
			<td>${pagina.titolo}</td>
			<td>${pagina.autore}</td>
			<td>${pagina.dataIns}</td>
		</tr>
		<tr>
			<td colspan="3">Testo</td>
		</tr>
		<tr>
			<td colspan="3">${pagina.testo}</td>
		</tr>

	</table>
	<br>
	<br>
	<br>
	<center>
		<jsp:include page="../commenti.jsp"></jsp:include>
		<span>Inserisci un commento</span>
		<form id="formCommento" method="post" action="CommentoController">
			<input type="hidden" name="aggiungiCommento" value="${pagina.idpagina}">
			
			<textarea rows="10" cols="50" name="testoc" placeholder="Testo del commento..."></textarea>
			<br>
			<input type="submit">
		</form>
		<br> <br>
	</center>
</body>
</html>